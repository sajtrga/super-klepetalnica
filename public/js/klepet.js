// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika


var Klepet = function(socket) {
  this.socket = socket;
};

var iMax = 100;
var jMax = 2;
var preimenovalnatabela = new Array();

for (indf=0;indf<iMax;indf++) {
 preimenovalnatabela[indf]=new Array();
 for (jndf=0;jndf<jMax;jndf++) {
  preimenovalnatabela[indf][jndf]="";
 }
}
var indekspreimenovanja = 0;
/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};


/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
case 'barva':
      besede.shift();
      var barva = besede.join(' ');
var kanalkanal = document.querySelector('#kanal');
var sporocill = document.querySelector('#sporocila');
//barva = '"' + barva + '"';
console.log(barva);
kanalkanal.style.backgroundColor = barva;
sporocill.style.backgroundColor = barva;
      break;

case 'preimenuj':
      besede.shift();
      var besediloo = besede.join(' ');
      var parametrii = besediloo.split('\"');
      if (parametrii) {
        //this.socket.emit('sporocilo', {vzdevek: parametri[1], besedilo: parametri[3]});
        sporocilo = 'Preimenoval uporabnika ' + parametrii[1] + ' v ' + parametrii[3];
	preimenovalnatabela[indekspreimenovanja][0] = parametrii[1];
	preimenovalnatabela[indekspreimenovanja][1] = parametrii[3];
	indekspreimenovanja++;
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;
    case 'zasebno':
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      if (parametri) {
	var novivzdevk = parametri[1];
	for(var privindx = 0; privindx < 100; privindx++){
		if(preimenovalnatabela[privindx][1] == parametri[1]){
			novivzdevk = preimenovalnatabela[privindx][0];
		}
	}
        this.socket.emit('sporocilo', {vzdevek: novivzdevk, besedilo: parametri[3]});
        sporocilo = '(zasebno za ' + parametri[1] + '): ' + parametri[3];
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;
case 'krcni':
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      if (parametri) {
var vzdev = parametri[1];
console.log(vzdev);
if(parametri[1].includes("(")){
 var vzdevv = vzdev.split("(");
var vzdevvv = vzdevv[vzdevv.length-1];
var vzdevvvv = vzdevvv.substring(0,vzdevvv.length - 1);
vzdev = vzdevvvv;
}
console.log(vzdev);
        this.socket.emit('krcanje', {vzdevek: vzdev, besedilo: "&#9756;"});
        sporocilo = 'Krcnil si osebo ' + parametri[1];
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;

    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};
